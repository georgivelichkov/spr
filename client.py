import socket

from helpers.utils import receiveResponse

HOST = '127.0.0.1'
PORT = 3001

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s_socket:
    s_socket.connect((HOST, PORT))
    while True:
        message = receiveResponse(s_socket)
        print(message)

        username = input("Enter username:")
        password = input("Enter password:")

        credentials = username + "," + password
        s_socket.send(credentials.encode("utf-8"))

        message = receiveResponse(s_socket)
        print(message)

        if "Show all blocked" in message:
            answer = input("Enter answer:")
            s_socket.send(answer.encode("utf-8"))
            message = receiveResponse(s_socket)
            print(message)
            s_socket.close()

