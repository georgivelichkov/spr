import os
import re
import socket

from helpers.file_manager import writeToLog
from helpers.utils import isLoginValid, printAllBlockedUsers, getUserByUsername, loadUsersFromFile, updateUser, \
    receiveResponse

# Global variables
HOST = '127.0.0.1'
PORT = 3001

# Load all users information from files as a list
loadUsersFromFile()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s_socket:
    s_socket.bind((HOST, PORT))
    s_socket.listen(10)
    print("Server started!")

    c_socket, address = s_socket.accept()

    with c_socket:
        c_socket.send(b"Enter credentials for login!\n")
        while True:
            credentials = receiveResponse(c_socket).replace(" ", "")
            data = re.compile("\\s*,\\s*").split(credentials)
            user = getUserByUsername(data[0])
            if isLoginValid(data[0], data[1]) and user.status != "blocked":
                message = "User with username:" + user.username + " has successfully logged in!"
                writeToLog("logs/access.log", message)

                user.validLogins = int(user.validLogins) + 1
                updateUser(user)
                msg = "Hello," + user.username + "\n"
                c_socket.send(msg.encode("utf-8"))

                if user.userRole == 'ADMIN':
                    c_socket.send(b"Show all blocked users?\n")

                    answer = receiveResponse(c_socket).replace(" ", "")
                    if answer == "Y":
                        printAllBlockedUsers(c_socket)
            else:
                c_socket.send(b"\n\nInvalid login!!")
                message = "Unsuccessful login from ip: " + address + " for username: " + user.username + "!"
                user.invalidLogins += int(user.invalidLogins) + 1
                if int(user.invalidLogins) >= 5:
                    user.status = "suspicious"

                if int(user.invalidLogins) >= 10:
                    user.status = "blocked"

                updateUser(user)

                writeToLog("logs/access.log", message)
