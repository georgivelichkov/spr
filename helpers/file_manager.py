# Read a file and return it as a string
def readFile(filename):
    with open(filename, 'r') as f:
        data = f.read()
    return data


# Write messages to log files
def writeToLog(filename, message):
    with open(filename, 'a') as file:
        file.write(str(message) + '\n')


# Write content to file
def writeToFile(filename, content):
    with open(filename, 'w') as file:
        file.write(str(content) + '\n')
