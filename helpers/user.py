class User:
    'Common base class for all users'

    def __init__(self, username, password, userIp, validLogins, invalidLogins, status, userRole):
        self.username = username
        self.password = password
        self.userIp = userIp
        self.validLogins = validLogins
        self.invalidLogins = invalidLogins
        self.status = status
        self.userRole = userRole

    def displayUserInfo(self):
        print("User IP:", self.userIp, "|Username:", self.username, "|Valid logins:", self.validLogins,
              "|Invalid Logins: ", self.invalidLogins, "|User status", self.status, "|User Role:", self.userRole)

    def getUserInfoWithHeaders(self):
        return "User IP:", self.userIp, "|Username:", self.username, "|Valid logins:", str(self.validLogins), \
               "|Invalid Logins: ", str(self.invalidLogins), "|User status", self.status, "|User Role:", self.userRole

    def getUserInfo(self):
        return self.userIp + "," + self.username + "," + str(self.validLogins) + "," + \
               str(self.invalidLogins) + "," + self.status + "," + self.userRole
