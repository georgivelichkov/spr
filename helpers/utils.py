import re

from helpers.user import User
from helpers.file_manager import readFile, writeToFile

users_headers = "ip, username, validLogins, invalidLogins, status, userRole"
users = []
BUFFER_SIZE = 1024

"""Validate login credentials.

@:param username - Username of the user
@:param password - Password of the user
"""


def isLoginValid(username, password):
    filteredUsersList = list(filter(lambda x: x.username == username and x.password == password, users))

    return len(filteredUsersList) > 0


# Load all users information from files as a list
def loadUsersFromFile():
    lines = re.compile("\\s*\n\\s*").split(readFile("config/usernames_ip.csv"))

    for line in lines[1:]:
        data = re.compile("\\s*,\\s*").split(line)

        if len(data) > 1:
            password = findPasswordForUsername(data[1])
            user = User(data[1], password, data[0], data[2], data[3], data[4], data[5])
            users.append(user)


"""Find user password in passwords file.

@:param username - Username of the user
"""


def findPasswordForUsername(username):
    password = ""

    lines = re.compile("\\s*\n\\s*").split(readFile("config/user_passwords.csv"))
    for line in lines[1:]:
        data = re.compile("\\s*,\\s*").split(line)
        if len(data) > 1 and data[0] == username:
            password = data[1]
            break

    return password


"""Update user information for a certain user.

@:param user - New updated user information.
"""


def updateUser(user):
    filteredUser = list(filter(lambda x: x.username == user.username, users))
    if len(filteredUser) > 0:
        index = users.index(filteredUser[0])
        users.remove(filteredUser[0])
        users.insert(index, user)

    usersList = "\n".join(getAllUsersInformation())
    content = users_headers + "\n" + usersList
    writeToFile("/Users/gvbox/PycharmProjects/spr-proekt/config/usernames_ip.csv", content)


"""Get all information for user with a certain username.

@:param username - Username of the user
"""


def getUserByUsername(username):
    user = list(filter(lambda x: x.username == username, users))
    if len(user) > 0: return user[0]


# Print all the information regarding blocked users.
def printAllBlockedUsers(c_socket):
    blockedUsers = list(filter(lambda x: x.status == "blocked", users))
    if len(blockedUsers) > 0:
        for blockedUser in blockedUsers:
            c_socket.send(blockedUser.getUserInfoWithHeaders())
    else:
        c_socket.send(b"No blocked users found!")


# Get all information regarding users for users file rewrite.
def getAllUsersInformation():
    result = []
    for user in users:
        result.append(user.getUserInfo())

    return result


# Get response from socket client
def receiveResponse(conn):
    data = conn.recv(BUFFER_SIZE)
    if not data: return ''
    return data.decode("utf-8")
